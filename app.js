angular.module('app', ['ngRoute', 'ui.grid', 'ui.grid.edit', 'ui.grid.autoFitColumns', 'ui.grid.pagination', 'ui.grid.resizeColumns'])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "main.html",
                controller: "MainCtrl",
                activetab: 'dashboard'
            })
            .when("/customers", {
                templateUrl: "customers.html",
                controller: "CustomerCtrl",
                activetab: 'customer'
            })
            .when("/drivers", {
                templateUrl: "drivers.html",
                controller: "DriverCtrl",
                activetab: 'driver'
            })
            .when("/affiliates", {
                templateUrl: "affiliates.html",
                controller: 'AffiliateCtrl',
                activetab: 'affiliate'
            });
    })
    .controller('navCtrl', ['$scope', '$http', '$route', function ($scope, $http, $route) {
        $scope.$route = $route;
    }])
    .controller('MainCtrl', ['$scope', '$http', '$route', function ($scope, $http, $route) {
        var data = [{
            "name": "Lesa",
            "age": 30,
            "gender": "female",
            "location": "Europe",
            "city": "London",
            "maritalStatus": "Married",
            "occupation": "Software Engineer"
        },
            {
                "name": "Bettye",
                "age": 29,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Contreras",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Cameron",
                "age": 28,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Robin",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Brenda",
                "age": 25,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Fleming",
                "age": 26,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Cathryn",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Hyde",
                "age": 25,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Sarah",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Alston",
                "age": 28,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Conrad",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Garza",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Charles",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Herminia",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Joy",
                "age": 27,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Meredith",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Leach",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Sosa",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Savage",
                "age": 29,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Morrison",
                "age": 25,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Frankie",
                "age": 29,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Annabelle",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Bonnie",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Dina",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Marva",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Jennings",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Barbara",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Bruce",
                "age": 27,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Albert",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Elvira",
                "age": 29,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Araceli",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Bullock",
                "age": 20,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Victoria",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Matthews",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Case",
                "age": 28,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Amie",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Ruby",
                "age": 29,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Linda",
                "age": 28,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Tracey",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Diaz",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Priscilla",
                "age": 24,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Dixie",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Sharron",
                "age": 22,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Deidre",
                "age": 21,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Potter",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Mcdonald",
                "age": 29,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Hartman",
                "age": 30,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Cook",
                "age": 30,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Powell",
                "age": 20,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Janette",
                "age": 30,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Mcfarland",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Lorena",
                "age": 24,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Bright",
                "age": 28,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Ester",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Marks",
                "age": 30,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Christa",
                "age": 30,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Gray",
                "age": 26,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Benjamin",
                "age": 25,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Lacy",
                "age": 28,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Belinda",
                "age": 28,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Diane",
                "age": 23,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Gross",
                "age": 30,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Wise",
                "age": 27,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Dunlap",
                "age": 26,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Ferrell",
                "age": 22,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Durham",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Gertrude",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Marla",
                "age": 25,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Kemp",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Murray",
                "age": 21,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Naomi",
                "age": 20,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Love",
                "age": 20,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Angelita",
                "age": 24,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Zamora",
                "age": 20,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Keri",
                "age": 28,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Pruitt",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Joyce",
                "age": 28,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Lula",
                "age": 24,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Hall",
                "age": 20,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Head",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Haynes",
                "age": 22,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Craig",
                "age": 27,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Baxter",
                "age": 26,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Delores",
                "age": 25,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Bernard",
                "age": 24,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Desiree",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Chrystal",
                "age": 24,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Stokes",
                "age": 26,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Griffin",
                "age": 23,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Lawanda",
                "age": 27,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Ford",
                "age": 27,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Lucile",
                "age": 26,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Carroll",
                "age": 30,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Skinner",
                "age": 28,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Celina",
                "age": 25,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Pope",
                "age": 29,
                "gender": "male",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Leah",
                "age": 29,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Rosie",
                "age": 25,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            },
            {
                "name": "Miriam",
                "age": 27,
                "gender": "female",
                "location": "Europe",
                "city": "London",
                "maritalStatus": "Married",
                "occupation": "Software Engineer"
            }
        ];

        $scope.gridOptions = {
            enableFiltering: true,
            enableGridMenu: true,
            rowHeight: 20,

            columnDefs: [{
                name: 'name',
            },
                {
                    name: 'age'
                },
                {
                    name: 'gender'
                },
                {
                    name: 'location'
                },
                {
                    name: 'city'
                },
                {
                    name: 'maritalStatus'
                },
                {
                    name: 'occupation'
                }
            ],
            data: data,
        };

    }])
    .controller('DriverCtrl', ['$scope', '$http', '$route', function ($scope, $http, $route) {
        var data = [
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JANE DOE",
                'SCHOOL CERT': "EXP",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '21/3/18',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JANE DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JANE DOE",
                'SCHOOL CERT': "EXP",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "EXP",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JANE DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JANE DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "EXP",
                'COACH CCERT': "",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "EXP",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            },
            {
                'DRIVER': "JOHN DOE",
                'SCHOOL CERT': "3/4/18",
                'MTN CERT': "3/4/18",
                'COACH CCERT': "5/17/18",
                'PARTY CERT': '',
                'FULL TIME': '',
                'PART TIME': '',
                'NIGHTS': '',
                'WEEK ENDS': '',
                'OUT OF TOWN': '',
                'PHONE': '555-333-2222',
                'EMAIL': 'ABC@AMTRANS.US',
                'HOURS': '1234',
                'HOURS12': '2468',
                'ACCIDENTS': 0,
                'HOURLY RATE': '$20.0',
                'NOTES': 'TEAM PLAYER'
            }
        ];

        $scope.driverGridOptions = {
            showGridFooter: true,
            columnDefs: [
                {name: 'DRIVER'},
                {
                    name: 'SCHOOL CERT',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '' || grid.getCellValue(row, col) == 'EXP') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'MTN CERT',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'COACH CERT',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PARTY CERT',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'FULL TIME',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PART TIME',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'NIGHTS',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'WEEK ENDS',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'OUT OF TOWN',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {name: 'PHONE'},
                {name: 'EMAIL'},
                {name: 'HOURS'},
                {name: 'HOURS12'},
                {name: 'ACCIDENTS'},
                {name: 'HOURLY RATE'},
                {name: 'NOTES'}
            ],
            data: data
        };

    }])
    .controller('CustomerCtrl', ['$scope', '$http', '$route', function ($scope, $http, $route) {
        var data = [
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'Y',
                'FARM': 'Y',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'Y',
                'FARM': 'N',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'N',
                'FARM': 'Y',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'Y',
                'FARM': 'N',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'Y',
                'FARM': 'N',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
            {
                'Customer': 'LAUSD',
                'SubCustomer': 'WASHINGTON HIGH',
                'FirstName': 'John',
                'LastName': 'Doe',
                'Contact': '555-333-2222',
                'AccountingContactFName': 'Bob',
                'AccountingContactLName': 'Johnson',
                'AccountingcontactPhone': '555-333-2222',
                'MonthRevenue': '$1.234,687',
                'AmountPastDue': '$323,333',
                'PaymentTerms': 'NET 30',
                'SPAB': 'Y',
                'FARM': 'N',
                'NOTES': 'VIP Client! CONTRACT EXPIRES 12/31/17'
            },
        ];
        $scope.customerGridOptions = {
            columnDefs: [
                {
                    name: 'Customer'
                },
                {
                    name: 'SubCustomer'
                },
                {
                    name: 'FirstName'
                },
                {
                    name: 'LastName'
                },
                {
                    name: 'Contact'
                },
                {
                    name: 'AccountingContactFName'
                },
                {
                    name: 'AccountingContactLName'
                },
                {
                    name: 'AccountingcontactPhone'
                },
                {
                    name: 'MonthRevenue'
                },
                {
                    name: 'AmountPastDue'
                },
                {
                    name: 'PaymentTerms'
                },
                {
                    name: 'SPAB',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row, col) == 'N') {
                            return 'pink';
                        }
                        return 'green'
                    }
                },
                {
                    name: 'FARM',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row, col) == 'N') {
                            return 'pink';
                        }
                        return 'green'
                    }
                },
                {
                    name: 'NOTES'
                },],
            data: data
        };
    }])
    .controller('AffiliateCtrl', ['$scope', '$http', '$route', function ($scope, $http, $route) {
        var data = [
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'N',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '0',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '60',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '2',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '10',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'N',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '0',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '5',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '2',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '12',
                'PFiveHrRate': '$20',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },
            {
                'Affiliates': 'CoachMan',
                'Rating': 'Rating',

                'STotal': '5',
                'SSpab': '3',
                'SFiveHrRate': '$300',

                'CTotal': '2',
                'CSpab': '0',
                'CFiveHrRate': '$400',

                'PTotal': '0',
                'PSpab': '0',
                'PFiveHrRate': '$0',

                'MTotal': '0',
                'MSpab': '0',
                'MFiveHrRate': '$0',

                'Phone1': '555-333-2222',
                'Phone2': '',

                'Contact1': 'John Doe',
                'Contact2': 'John Smith',

                'Email': 'ABC@AMTRNUS.US',
                'PucNo': 'No 12345',
                'SignedTerms': 'Y',
                'InsExp': '5/5/18',
                'Notes': 'Old Buses',
            },

        ];

        $scope.affilatedGridOptions = {
            showGridFooter: true,
            columnDefs: [
                {
                    name: 'Affiliates'
                },
                {
                    name: 'Rating'
                },

                {
                    name: 'STotal',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'SSpab',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'SFiveHrRate',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },

                {
                    name: 'PTotal',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PSpab',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PFiveHrRate',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },

                {
                    name: 'PTotal',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PSpab',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'PFiveHrRate',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },

                {
                    name: 'MTotal',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'MSpab',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'MFiveHrRate',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == '$0') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },

                {
                    name: 'Phone1'
                },
                {
                    name: 'Phone2'
                },

                {
                    name: 'Contact1'
                },
                {
                    name: 'Contact2'
                },

                {
                    name: 'Email'
                },
                {
                    name: 'PucNo'
                },
                {
                    name: 'SignedTerms',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        console.log(grid.getCellValue(row, col));
                        if (grid.getCellValue(row, col) == 'N') {
                            return 'pink';
                        }
                        return 'green';
                    }
                },
                {
                    name: 'InsExp'
                },
                {
                    name: 'Notes'
                }

            ],
            data
        };

    }]);