const express = require('express');
const app = express();
const path = require('path');
const gzipStatic = require('connect-gzip-static');

// Run the app by serving the static files
// in the dist directory
app.use(gzipStatic(__dirname + '/'));

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080);